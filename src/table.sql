SET SESSION FOREIGN_KEY_CHECKS=0;

/* Drop Tables */

DROP TABLE IF EXISTS fadingdaibiaorengongzuolvli;
DROP TABLE IF EXISTS gaoguanqingkuang;
DROP TABLE IF EXISTS shixinghoujijin;
DROP TABLE IF EXISTS shixingqianjijin;
DROP TABLE IF EXISTS base_message;




/* Create Tables */

CREATE TABLE base_message
(
	chnegxinxinxi varchar(500),
	-- 中文名
	chineseName varchar(100) NOT NULL COMMENT '中文名',
	-- 英文名
	englishName varchar(300) COMMENT '英文名',
	dengjibianhao varchar(20),
	jigoudaima varchar(20),
	dengjitime varchar(12),
	chenglitime varchar(12),
	zhucedi varchar(200),
	bangongdi varchar(200),
	-- 万元 人民币
	zhuceziben varchar(10) COMMENT '万元 人民币',
	shijiaoziben varchar(10),
	qiyexinzhi varchar(12),
	zhucezibenshijiaobili varchar(10),
	jigouleixin varchar(80),
	yewuleixin varchar(30),
	yuangongrenshu varchar(10),
	jigouwangzhi varchar(200),
	shifouhuiyuan varchar(2),
	dangqianhuiyuanleix varbinary(12),
	ruhuitime varchar(12),
	falvyijian varchar(200),
	lvshishiwusuomingcheng varchar(200),
	lvshixingming varchar(200),
	fadingrenname varchar(30),
	shifoucongyezige varchar(200),
	zigequdefangshi varchar(20),
	jigouzuihougengxintime varchar(12),
	tebietishixinxi varchar(200),
	PRIMARY KEY (chineseName)
);


CREATE TABLE fadingdaibiaorengongzuolvli
(
	time varchar(100),
	danwei varchar(100),
	zhiwu varchar(50),
	-- 中文名
	chineseName varchar(100) NOT NULL COMMENT '中文名'
);


CREATE TABLE gaoguanqingkuang
(
	name varchar(20),
	zhiwu varchar(200),
	shifouyoucongyezige varchar(200),
	-- 中文名
	chineseName varchar(100) NOT NULL COMMENT '中文名'
);


CREATE TABLE shixinghoujijin
(
	name1 varchar(200),
	yuebao varchar(50),
	jibao varchar(50),
	bannianbao varchar(50),
	nianbao varchar(50),
	-- 中文名
	chineseName varchar(100) NOT NULL COMMENT '中文名'
);


CREATE TABLE shixingqianjijin
(
	name1 varchar(200),
	yuebao varchar(50),
	jibao varchar(50),
	bannianbao varchar(50),
	nianbao varchar(50),
	-- 中文名
	chineseName varchar(100) NOT NULL COMMENT '中文名'
);



/* Create Foreign Keys */

ALTER TABLE fadingdaibiaorengongzuolvli
	ADD FOREIGN KEY (chineseName)
	REFERENCES base_message (chineseName)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE gaoguanqingkuang
	ADD FOREIGN KEY (chineseName)
	REFERENCES base_message (chineseName)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE shixinghoujijin
	ADD FOREIGN KEY (chineseName)
	REFERENCES base_message (chineseName)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE shixingqianjijin
	ADD FOREIGN KEY (chineseName)
	REFERENCES base_message (chineseName)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;



