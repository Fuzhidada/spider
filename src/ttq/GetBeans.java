package ttq;

import bean.*;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.LinkedHashMap;

public class GetBeans {
    Busdao b = new Busdao();

    public GetBeans() {
    }

    public void getall(LinkedHashMap<String, String> m) {
        for (String key : m.keySet()) {
            BufferedReader in = null;
            try {
                URL urlx = new URL(
                        "http://gs.amac.org.cn/amac-infodisc/res/pof/manager/"
                                + m.get(key) + "");

                URLConnection conn = urlx.openConnection();
                in = new BufferedReader(new InputStreamReader(
                        conn.getInputStream(), "UTF-8"));
                String line = null;
                String htmls = "";
                while ((line = in.readLine()) != null) {
                    htmls += line;
                }

                BaseMessage basem = new BaseMessage();
                Document doc = Jsoup.parse(htmls);
                // start BaseMessage
                Element e1 = doc.getElementsByClass(
                        "table table-center table-info").last();
                basem.setChnegxinxinxi(e1.text());

                Element e2 = doc.getElementById("complaint1");
                basem.setChinesename(e2.text());

                Element e3 = doc.getElementsByClass("td-content").get(2);
                basem.setEnglishname(e3.text()); // 英文名

                Element e4 = doc.getElementsByClass("td-content").get(3);
                basem.setDengjibianhao(e4.text());

                Element e5 = doc.getElementsByClass("td-content").get(4);
                basem.setJigoudaima(e5.text());

                Element e6 = doc.getElementsByClass("td-content").get(5);
                basem.setDengjitime(e6.text());

                Element e7 = doc.getElementsByClass("td-content").get(6);
                basem.setChenglitime(e7.text());

                Element e8 = doc.getElementsByClass("td-content").get(7);
                basem.setZhucedi(e8.text()); // 注册地

                Element e9 = doc.getElementsByClass("td-content").get(8);
                basem.setBangongdi(e9.text());

                Element e10 = doc.getElementsByClass("td-content").get(9);
                basem.setZhuceziben(e10.text());

                Element e11 = doc.getElementsByClass("td-content").get(10);
                basem.setShijiaoziben(e11.text()); // 实缴资本

                Element e12 = doc.getElementsByClass("td-content").get(11);
                basem.setQiyexinzhi(e12.text());

                Element e13 = doc.getElementsByClass("td-content").get(12);
                basem.setZhucezibenshijiaobili(e13.text());

                Element e14 = doc.getElementsByClass("td-content").get(13);
                basem.setJigouleixin(e14.text());

                Element e15 = doc.getElementsByClass("td-content").get(14);
                basem.setYewuleixin(e15.text());

                Element e16 = doc.getElementsByClass("td-content").get(15);
                basem.setYuangongrenshu(e16.text());

                Element e17 = doc.getElementsByClass("td-content").get(16);
                basem.setJigouwangzhi(e17.text());

                Element e18 = doc.getElementsByClass("td-content").get(17);
                basem.setShifouhuiyuan(e18.text());

                if (e18.text().equals("是")) {

                    Element e19 = doc.getElementsByClass("td-content").get(18);
                    basem.setDangqianhuiyuanleix(e19.text());

                    Element e20 = doc.getElementsByClass("td-content").get(19);
                    basem.setRuhuitime(e20.text());

                    Element e21 = doc.getElementsByClass("td-content").get(20);
                    basem.setFalvyijian(e21.text());     // 法律意见书状态

                    if (e21.text().trim().equals("")) {

                        Element e22 = doc.getElementsByClass("td-content").get(21);
                        basem.setFadingrenname(e22.text());

                        Element e23 = doc.getElementsByClass("td-content").get(22);
                        basem.setShifoucongyezige(e23.text());

                        Element e24 = doc.getElementsByClass("td-content").get(23);
                        basem.setZigequdefangshi(e24.text()); // 跳一个

                        Element e25 = doc.getElementsByClass("td-content").get(28);
                        basem.setJigouzuihougengxintime(e25.text());

                        Element e26 = doc.getElementsByClass("td-content").get(29);
                        basem.setTebietishixinxi(e26.text());

                        b.intsert1(basem);
                        // System.out.println(basem);

                        // //end BaseMessage

                        // start Fadingdaibiaorengongzuolvli

                        Elements trs = doc
                                .getElementsByClass("table table-center table-noborder")
                                .get(0).getElementsByTag("tbody").get(0).select("tr");

                        for (int i = 0; i < trs.size(); i++) {
                            Elements tds = trs.get(i).select("td");
                            Fadingdaibiaorengongzuolvli fa = new Fadingdaibiaorengongzuolvli();
                            fa.setBaseMessage(e2.text());
                            for (int j = 0; j < tds.size(); j++) {
                                switch (j) {
                                    case 0:
                                        fa.setTime(tds.get(j).text());
                                        break;
                                    case 1:
                                        fa.setDanwei(tds.get(j).text());
                                        break;
                                    case 2:
                                        fa.setZhiwu(tds.get(j).text());
                                        break;
                                    default:
                                        break;
                                }

                            }
                            b.intsert2(fa);
                            // System.out.println(fa);
                        }
                        // end Fadingdaibiaorengongzuolvli

                        Elements trs1 = doc
                                .getElementsByClass("table table-center table-noborder")
                                .get(1).getElementsByTag("tbody").get(0).select("tr");

                        for (int i = 0; i < trs1.size(); i++) {
                            Elements tds1 = trs1.get(i).select("td");
                            Gaoguanqingkuang gao = new Gaoguanqingkuang();
                            gao.setBaseMessage(e2.text());

                            for (int j = 0; j < tds1.size(); j++) {
                                switch (j) {
                                    case 0:
                                        gao.setName(tds1.get(0).text());
                                        break;
                                    case 1:
                                        gao.setZhiwu(tds1.get(1).text());
                                        break;
                                    case 2:
                                        gao.setShifouyoucongyezige(tds1.get(2).text().trim().substring(0, 1));
                                        break;
                                    default:
                                        break;
                                }

                            }
                            b.intsert3(gao);
                        }
                        //end 高管信息

                        //start 暂行办法实施前成立的基金
                        Elements ps = doc.getElementsByClass("td-content").get(26).getElementsByTag("p");
//				System.out.println(ps.size());


                        for (int i = 0; i < ps.size(); i += 2) {
                            Shixingqianjijin shixin = new Shixingqianjijin();
                            shixin.setBaseMessage(e2.text());
                            shixin.setName1(ps.get(i).text());
                            String xx = ps.get(i + 1).text();
//				System.out.println(xx);
                            String[] xxl = xx.split("；");
                            for (int j = 0; j < xxl.length; j++) {
                                switch (j) {
                                    case 0:
                                        shixin.setYuebao(xxl[0].split("：")[1].substring(3, 4));
                                        break;
                                    case 1:
                                        shixin.setJibao(xxl[1].split("：")[1].substring(3, 4));
                                        break;
                                    case 2:
                                        shixin.setBannianbao(xxl[2].split("：")[1].substring(3, 4));
                                        break;
                                    case 3:
                                        shixin.setNianbao(xxl[3].split("：")[1].substring(3, 4));
                                        break;

                                    default:
                                        break;
                                }
                            }
                            b.intsert4(shixin);
//				System.out.println(shixin);

                        }
                        //end 暂行办法实施前成立的基金


                        //start 暂行办法实施     后 成立的基金
                        Elements ps1 = doc.getElementsByClass("td-content").get(27).getElementsByTag("p");

                        for (int i = 0; i < ps1.size(); i += 2) {
                            Shixinghoujijin shixin1 = new Shixinghoujijin();
                            shixin1.setBaseMessage(e2.text());
                            shixin1.setName1(ps1.get(i).text());
                            String xx1 = ps1.get(i + 1).text();
//				System.out.println(xx1);
                            String[] xxl = xx1.split("；");
                            for (int j = 0; j < xxl.length; j++) {
                                switch (j) {
                                    case 0:
                                        shixin1.setYuebao(xxl[0].split("：")[1].substring(3, 4));
                                        break;
                                    case 1:
                                        shixin1.setJibao(xxl[1].split("：")[1].substring(3, 4));
                                        break;
                                    case 2:
                                        shixin1.setBannianbao(xxl[2].split("：")[1].substring(3, 4));
                                        break;
                                    case 3:
                                        shixin1.setNianbao(xxl[3].split("：")[1].substring(3, 4));
                                        break;

                                    default:
                                        break;
                                }
                            }
//			System.out.println(shixin1);
                            b.intsert5(shixin1);
                        }
                        //end 暂行办法实施     后 成立的基金
                    }
                    //   是会员  法律意见状态：办结
                    else {
                        if (e21.text().trim().equals("办结")) {
                            System.out.println(e2.text() + m.get(key));
//   keng

                            Element e221 = doc.getElementsByClass("td-content").get(21);
                            basem.setLvshishiwusuomingcheng(e221.text());

                            Element e232 = doc.getElementsByClass("td-content").get(22);
                            basem.setLvshixingming(e232.text());


                            Element e22 = doc.getElementsByClass("td-content").get(23);
                            basem.setFadingrenname(e22.text());

                            Element e23 = doc.getElementsByClass("td-content").get(24);
                            basem.setShifoucongyezige(e23.text());
                            // ??????出问题
                            Element e24 = doc.getElementsByClass("td-content").get(25);
                            basem.setZigequdefangshi(e24.text()); // 跳一个

                            Element e25 = doc.getElementsByClass("td-content").get(30);
                            basem.setJigouzuihougengxintime(e25.text());

                            Element e26 = doc.getElementsByClass("td-content").get(31);
                            basem.setTebietishixinxi(e26.text());

                            int x1 = b.intsert1(basem);
//						System.out.println("bean   "+ x1);
                            // System.out.println(basem);

                            // //end BaseMessage

                            // start Fadingdaibiaorengongzuolvli

                            Elements trs = doc
                                    .getElementsByClass("table table-center table-noborder")
                                    .get(0).getElementsByTag("tbody").get(0).select("tr");

                            for (int i = 0; i < trs.size(); i++) {
                                Elements tds = trs.get(i).select("td");
                                Fadingdaibiaorengongzuolvli fa = new Fadingdaibiaorengongzuolvli();
                                fa.setBaseMessage(e2.text());
                                for (int j = 0; j < tds.size(); j++) {
                                    switch (j) {
                                        case 0:
                                            fa.setTime(tds.get(j).text());
                                            break;
                                        case 1:
                                            fa.setDanwei(tds.get(j).text());
                                            break;
                                        case 2:
                                            fa.setZhiwu(tds.get(j).text());
                                            break;
                                        default:
                                            break;
                                    }

                                }
                                b.intsert2(fa);
                                // System.out.println(fa);
                            }
                            // end Fadingdaibiaorengongzuolvli

                            Elements trs1 = doc
                                    .getElementsByClass("table table-center table-noborder")
                                    .get(1).getElementsByTag("tbody").get(0).select("tr");

                            for (int i = 0; i < trs1.size(); i++) {
                                Elements tds1 = trs1.get(i).select("td");
                                Gaoguanqingkuang gao = new Gaoguanqingkuang();
                                gao.setBaseMessage(e2.text());

                                for (int j = 0; j < tds1.size(); j++) {
                                    switch (j) {
                                        case 0:
                                            gao.setName(tds1.get(0).text());
                                            break;
                                        case 1:
                                            gao.setZhiwu(tds1.get(1).text());
                                            break;
                                        case 2:
                                            gao.setShifouyoucongyezige(tds1.get(2).text().trim().substring(0, 1));
                                            break;
                                        default:
                                            break;
                                    }

                                }
                                b.intsert3(gao);
                            }
                            //end 高管信息

                            //start 暂行办法实施前成立的基金
                            Elements ps = doc.getElementsByClass("td-content").get(28).getElementsByTag("p");
//						System.out.println(ps.size());


                            for (int i = 0; i < ps.size(); i += 2) {
                                Shixingqianjijin shixin = new Shixingqianjijin();
                                shixin.setBaseMessage(e2.text());
                                shixin.setName1(ps.get(i).text());
                                String xx = ps.get(i + 1).text();
//						System.out.println(xx);
                                String[] xxl = xx.split("；");
                                for (int j = 0; j < xxl.length; j++) {
                                    switch (j) {
                                        case 0:
                                            shixin.setYuebao(xxl[0].split("：")[1].substring(3, 4));
                                            break;
                                        case 1:
                                            shixin.setJibao(xxl[1].split("：")[1].substring(3, 4));
                                            break;
                                        case 2:
                                            shixin.setBannianbao(xxl[2].split("：")[1].substring(3, 4));
                                            break;
                                        case 3:
                                            shixin.setNianbao(xxl[3].split("：")[1].substring(3, 4));
                                            break;

                                        default:
                                            break;
                                    }
                                }
                                b.intsert4(shixin);
//						System.out.println(shixin);

                            }
                            //end 暂行办法实施前成立的基金


                            //start 暂行办法实施     后 成立的基金
                            Elements ps1 = doc.getElementsByClass("td-content").get(29).getElementsByTag("p");
//						System.out.println(ps1.size());
                            for (int i = 0; i < ps1.size(); i += 2) {
                                Shixinghoujijin shixin1 = new Shixinghoujijin();
                                shixin1.setBaseMessage(e2.text());
                                shixin1.setName1(ps1.get(i).text());
                                String xx1 = ps1.get(i + 1).text();
//						System.out.println(xx1);
                                String[] xxl = xx1.split("；");
                                for (int j = 0; j < xxl.length; j++) {
                                    switch (j) {
                                        case 0:
                                            shixin1.setYuebao(xxl[0].split("：")[1].substring(3, 4));
                                            break;
                                        case 1:
                                            shixin1.setJibao(xxl[1].split("：")[1].substring(3, 4));
                                            break;
                                        case 2:
                                            shixin1.setBannianbao(xxl[2].split("：")[1].substring(3, 4));
                                            break;
                                        case 3:
                                            shixin1.setNianbao(xxl[3].split("：")[1].substring(3, 4));
                                            break;

                                        default:
                                            break;
                                    }
                                }
//					System.out.println(shixin1);
                                int a = b.intsert5(shixin1);
//				System.out.println(a);
                            }
                            //end 暂行办法实施     后 成立的基金


                        }

                    }

                } else {


                    Element e21 = doc.getElementsByClass("td-content").get(18);
                    basem.setFalvyijian(e21.text()); // 法律意见书状态

                    if (e21.text().trim().equals("办结")) {

                        //不是会员  办结

                        Element e221 = doc.getElementsByClass("td-content").get(19);
                        basem.setLvshishiwusuomingcheng(e221.text());

                        Element e232 = doc.getElementsByClass("td-content").get(20);
                        basem.setLvshixingming(e232.text());


                        Element e22 = doc.getElementsByClass("td-content").get(21);
                        basem.setFadingrenname(e22.text());

                        Element e23 = doc.getElementsByClass("td-content").get(22);
                        basem.setShifoucongyezige(e23.text());
                        // ??????出问题
                        Element e24 = doc.getElementsByClass("td-content").get(23);
                        basem.setZigequdefangshi(e24.text()); // 跳一个

                        Element e25 = doc.getElementsByClass("td-content").get(28);
                        basem.setJigouzuihougengxintime(e25.text());

                        Element e26 = doc.getElementsByClass("td-content").get(29);
                        basem.setTebietishixinxi(e26.text());

                        int x1 = b.intsert1(basem);
//						System.out.println("bean   "+ x1);
                        // System.out.println(basem);

                        // //end BaseMessage

                        // start Fadingdaibiaorengongzuolvli

                        Elements trs = doc
                                .getElementsByClass("table table-center table-noborder")
                                .get(0).getElementsByTag("tbody").get(0).select("tr");

                        for (int i = 0; i < trs.size(); i++) {
                            Elements tds = trs.get(i).select("td");
                            Fadingdaibiaorengongzuolvli fa = new Fadingdaibiaorengongzuolvli();
                            fa.setBaseMessage(e2.text());
                            for (int j = 0; j < tds.size(); j++) {
                                switch (j) {
                                    case 0:
                                        fa.setTime(tds.get(j).text());
                                        break;
                                    case 1:
                                        fa.setDanwei(tds.get(j).text());
                                        break;
                                    case 2:
                                        fa.setZhiwu(tds.get(j).text());
                                        break;
                                    default:
                                        break;
                                }

                            }
                            b.intsert2(fa);
                            // System.out.println(fa);
                        }
                        // end Fadingdaibiaorengongzuolvli

                        Elements trs1 = doc
                                .getElementsByClass("table table-center table-noborder")
                                .get(1).getElementsByTag("tbody").get(0).select("tr");

                        for (int i = 0; i < trs1.size(); i++) {
                            Elements tds1 = trs1.get(i).select("td");
                            Gaoguanqingkuang gao = new Gaoguanqingkuang();
                            gao.setBaseMessage(e2.text());

                            for (int j = 0; j < tds1.size(); j++) {
                                switch (j) {
                                    case 0:
                                        gao.setName(tds1.get(0).text());
                                        break;
                                    case 1:
                                        gao.setZhiwu(tds1.get(1).text());
                                        break;
                                    case 2:
                                        gao.setShifouyoucongyezige(tds1.get(2).text().trim().substring(0, 1));
                                        break;
                                    default:
                                        break;
                                }

                            }
                            b.intsert3(gao);
                        }
                        //end 高管信息

                        //start 暂行办法实施前成立的基金
                        Elements ps = doc.getElementsByClass("td-content").get(26).getElementsByTag("p");
//						System.out.println(ps.size());


                        for (int i = 0; i < ps.size(); i += 2) {
                            Shixingqianjijin shixin = new Shixingqianjijin();
                            shixin.setBaseMessage(e2.text());
                            shixin.setName1(ps.get(i).text());
                            String xx = ps.get(i + 1).text();
//						System.out.println(xx);
                            String[] xxl = xx.split("；");
                            for (int j = 0; j < xxl.length; j++) {
                                switch (j) {
                                    case 0:
                                        shixin.setYuebao(xxl[0].split("：")[1].substring(3, 4));
                                        break;
                                    case 1:
                                        shixin.setJibao(xxl[1].split("：")[1].substring(3, 4));
                                        break;
                                    case 2:
                                        shixin.setBannianbao(xxl[2].split("：")[1].substring(3, 4));
                                        break;
                                    case 3:
                                        shixin.setNianbao(xxl[3].split("：")[1].substring(3, 4));
                                        break;

                                    default:
                                        break;
                                }
                            }
                            b.intsert4(shixin);
//						System.out.println(shixin);

                        }
                        //end 暂行办法实施前成立的基金


                        //start 暂行办法实施     后 成立的基金
                        Elements ps1 = doc.getElementsByClass("td-content").get(27).getElementsByTag("p");
//						System.out.println(ps1.size());
                        for (int i = 0; i < ps1.size(); i += 2) {
                            Shixinghoujijin shixin1 = new Shixinghoujijin();
                            shixin1.setBaseMessage(e2.text());
                            shixin1.setName1(ps1.get(i).text());
                            String xx1 = ps1.get(i + 1).text();
//						System.out.println(xx1);
                            String[] xxl = xx1.split("；");
                            for (int j = 0; j < xxl.length; j++) {
                                switch (j) {
                                    case 0:
                                        shixin1.setYuebao(xxl[0].split("：")[1].substring(3, 4));
                                        break;
                                    case 1:
                                        shixin1.setJibao(xxl[1].split("：")[1].substring(3, 4));
                                        break;
                                    case 2:
                                        shixin1.setBannianbao(xxl[2].split("：")[1].substring(3, 4));
                                        break;
                                    case 3:
                                        shixin1.setNianbao(xxl[3].split("：")[1].substring(3, 4));
                                        break;

                                    default:
                                        break;
                                }
                            }
//					System.out.println(shixin1);
                            int a = b.intsert5(shixin1);
//				System.out.println(a);
                        }
                        //end 暂行办法实施     后 成立的基金


                    } else {
                        //不是会员  未办结
                        Element e22 = doc.getElementsByClass("td-content").get(19);
                        basem.setFadingrenname(e22.text());

                        Element e23 = doc.getElementsByClass("td-content").get(20);
                        basem.setShifoucongyezige(e23.text());
                        // ??????出问题
//					System.out.println(e23.text());
                        Element e24 = doc.getElementsByClass("td-content").get(21);
                        basem.setZigequdefangshi(e24.text()); // 跳一个

                        Element e25 = doc.getElementsByClass("td-content").get(26);
                        basem.setJigouzuihougengxintime(e25.text());

                        Element e26 = doc.getElementsByClass("td-content").get(27);
                        basem.setTebietishixinxi(e26.text());

                        int x1 = b.intsert1(basem);
//					System.out.println("bean   "+ x1);
                        // System.out.println(basem);

                        // //end BaseMessage

                        // start Fadingdaibiaorengongzuolvli

                        Elements trs = doc
                                .getElementsByClass("table table-center table-noborder")
                                .get(0).getElementsByTag("tbody").get(0).select("tr");

                        for (int i = 0; i < trs.size(); i++) {
                            Elements tds = trs.get(i).select("td");
                            Fadingdaibiaorengongzuolvli fa = new Fadingdaibiaorengongzuolvli();
                            fa.setBaseMessage(e2.text());
                            for (int j = 0; j < tds.size(); j++) {
                                switch (j) {
                                    case 0:
                                        fa.setTime(tds.get(j).text());
                                        break;
                                    case 1:
                                        fa.setDanwei(tds.get(j).text());
                                        break;
                                    case 2:
                                        fa.setZhiwu(tds.get(j).text());
                                        break;
                                    default:
                                        break;
                                }

                            }
                            b.intsert2(fa);
                            // System.out.println(fa);
                        }
                        // end Fadingdaibiaorengongzuolvli

                        Elements trs1 = doc
                                .getElementsByClass("table table-center table-noborder")
                                .get(1).getElementsByTag("tbody").get(0).select("tr");

                        for (int i = 0; i < trs1.size(); i++) {
                            Elements tds1 = trs1.get(i).select("td");
                            Gaoguanqingkuang gao = new Gaoguanqingkuang();
                            gao.setBaseMessage(e2.text());

                            for (int j = 0; j < tds1.size(); j++) {
                                switch (j) {
                                    case 0:
                                        gao.setName(tds1.get(0).text());
                                        break;
                                    case 1:
                                        gao.setZhiwu(tds1.get(1).text());
                                        break;
                                    case 2:
                                        gao.setShifouyoucongyezige(tds1.get(2).text().trim().substring(0, 1));
                                        break;
                                    default:
                                        break;
                                }

                            }
                            b.intsert3(gao);
                        }
                        //end 高管信息

                        //start 暂行办法实施前成立的基金
                        Elements ps = doc.getElementsByClass("td-content").get(24).getElementsByTag("p");
//					System.out.println(ps.size());


                        for (int i = 0; i < ps.size(); i += 2) {
                            Shixingqianjijin shixin = new Shixingqianjijin();
                            shixin.setBaseMessage(e2.text());
                            shixin.setName1(ps.get(i).text());
                            String xx = ps.get(i + 1).text();
//					System.out.println(xx);
                            String[] xxl = xx.split("；");
                            for (int j = 0; j < xxl.length; j++) {
                                switch (j) {
                                    case 0:
                                        shixin.setYuebao(xxl[0].split("：")[1].substring(3, 4));
                                        break;
                                    case 1:
                                        shixin.setJibao(xxl[1].split("：")[1].substring(3, 4));
                                        break;
                                    case 2:
                                        shixin.setBannianbao(xxl[2].split("：")[1].substring(3, 4));
                                        break;
                                    case 3:
                                        shixin.setNianbao(xxl[3].split("：")[1].substring(3, 4));
                                        break;

                                    default:
                                        break;
                                }
                            }
                            b.intsert4(shixin);
//					System.out.println(shixin);

                        }
                        //end 暂行办法实施前成立的基金


                        //start 暂行办法实施     后 成立的基金
                        Elements ps1 = doc.getElementsByClass("td-content").get(25).getElementsByTag("p");
//					System.out.println(ps1.size());
                        for (int i = 0; i < ps1.size(); i += 2) {
                            Shixinghoujijin shixin1 = new Shixinghoujijin();
                            shixin1.setBaseMessage(e2.text());
                            shixin1.setName1(ps1.get(i).text());
                            String xx1 = ps1.get(i + 1).text();
//					System.out.println(xx1);
                            String[] xxl = xx1.split("；");
                            for (int j = 0; j < xxl.length; j++) {
                                switch (j) {
                                    case 0:
                                        shixin1.setYuebao(xxl[0].split("：")[1].substring(3, 4));
                                        break;
                                    case 1:
                                        shixin1.setJibao(xxl[1].split("：")[1].substring(3, 4));
                                        break;
                                    case 2:
                                        shixin1.setBannianbao(xxl[2].split("：")[1].substring(3, 4));
                                        break;
                                    case 3:
                                        shixin1.setNianbao(xxl[3].split("：")[1].substring(3, 4));
                                        break;

                                    default:
                                        break;
                                }
                            }
//				System.out.println(shixin1);
                            int a = b.intsert5(shixin1);
//			System.out.println(a);
                        }
                        //end 暂行办法实施     后 成立的基金

                    }
                }


            } catch (Exception e) {
                System.out.println(m.get(key));
                e.printStackTrace();

            } finally {
                try {
                    if (in != null)
                        in.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
