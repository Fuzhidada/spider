package ttq;

import bean.*;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;

import java.sql.SQLException;

public class Busdao extends Basedao {

    public int intsert1(BaseMessage bas) {
        int result = 0;
        try {
            Connection c = getConnection();
            PreparedStatement ps1 = (PreparedStatement) c.prepareStatement("insert into base_message value(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

            ps1.setString(1, bas.getChnegxinxinxi());
            ps1.setString(2, bas.getChinesename());
            ps1.setString(3, bas.getEnglishname());
            ps1.setString(4, bas.getDengjibianhao());
            ps1.setString(5, bas.getJigoudaima());
            ps1.setString(6, bas.getDengjitime());
            ps1.setString(7, bas.getChenglitime());
            ps1.setString(8, bas.getZhucedi());
            ps1.setString(9, bas.getBangongdi());
            ps1.setString(10, bas.getZhuceziben());
            ps1.setString(11, bas.getShijiaoziben());
            ps1.setString(12, bas.getQiyexinzhi());
            ps1.setString(13, bas.getZhucezibenshijiaobili());
            ps1.setString(14, bas.getJigouleixin());
            ps1.setString(15, bas.getYewuleixin());
            ps1.setString(16, bas.getYuangongrenshu());
            ps1.setString(17, bas.getJigouwangzhi());
            ps1.setString(18, bas.getShifouhuiyuan());
            ps1.setString(19, bas.getDangqianhuiyuanleix());
            ps1.setString(20, bas.getRuhuitime());
            ps1.setString(21, bas.getFalvyijian());
            ps1.setString(22, bas.getLvshishiwusuomingcheng());
            ps1.setString(23, bas.getLvshixingming());
            ps1.setString(24, bas.getFadingrenname());
            ps1.setString(25, bas.getShifoucongyezige());
            ps1.setString(26, bas.getZigequdefangshi());
            ps1.setString(27, bas.getJigouzuihougengxintime());
            ps1.setString(28, bas.getTebietishixinxi());

            result = ps1.executeUpdate();
            ps1.close();
            c.close();
        } catch (Exception e) {
            System.out.println(bas.getChinesename());
            e.printStackTrace();
        }
        return result;
    }

    public int intsert2(Fadingdaibiaorengongzuolvli fa) {
        int result = 0;
        try {
            Connection c = getConnection();
            PreparedStatement ps2 = (PreparedStatement) c.prepareStatement("insert into fadingdaibiaorengongzuolvli value(?,?,?,?)");

            ps2.setString(1, fa.getTime());
            ps2.setString(2, fa.getDanwei());
            ps2.setString(3, fa.getZhiwu());
            ps2.setString(4, fa.getBaseMessage());

            result = ps2.executeUpdate();
            ps2.close();
            c.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return result;
    }

    public int intsert3(Gaoguanqingkuang ga) {
        int result = 0;
        try {
            Connection c = getConnection();
            PreparedStatement ps3 = (PreparedStatement) c.prepareStatement("insert into gaoguanqingkuang value(?,?,?,?)");
            ps3.setString(1, ga.getName());
            ps3.setString(2, ga.getZhiwu());
            ps3.setString(3, ga.getShifouyoucongyezige());
            ps3.setString(4, ga.getBaseMessage());
            result = ps3.executeUpdate();
            ps3.close();
            c.close();
        } catch (SQLException e) {
            System.out.println(ga.getName());
            e.printStackTrace();
        }

        return result;
    }

    public int intsert4(Shixingqianjijin sq) {
        int result = 0;
        try {
            Connection c = getConnection();
            PreparedStatement ps4 = (PreparedStatement) c.prepareStatement("insert into shixingqianjijin value(?,?,?,?,?,?)");
            ps4.setString(1, sq.getName1());
            ps4.setString(2, sq.getYuebao());
            ps4.setString(3, sq.getJibao());
            ps4.setString(4, sq.getBannianbao());
            ps4.setString(5, sq.getNianbao());
            ps4.setString(6, sq.getBaseMessage());
            result = ps4.executeUpdate();
            ps4.close();
            c.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return result;
    }

    public int intsert5(Shixinghoujijin sh) {
        int result = 0;
        try {
            Connection c = getConnection();
            PreparedStatement ps5 = (PreparedStatement) c.prepareStatement("insert into shixinghoujijin value(?,?,?,?,?,?)");
            ps5.setString(1, sh.getName1());
            ps5.setString(2, sh.getYuebao());
            ps5.setString(3, sh.getJibao());
            ps5.setString(4, sh.getBannianbao());
            ps5.setString(5, sh.getNianbao());
            ps5.setString(6, sh.getBaseMessage());
            result = ps5.executeUpdate();
            ps5.close();
            c.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return result;
    }

//
//	public void updataPage(int x){
//
//		try {
//			Connection c=getConnection();
//			PreparedStatement ps6=(PreparedStatement) c.prepareStatement("update page set page=?");
//			ps6.setInt(1, x);
//			ps6.executeUpdate();
//			ps6.close();
//			c.close();
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//	}

}
