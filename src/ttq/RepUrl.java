package ttq;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class RepUrl {
    public static String sendPost(String url, String param) {
        PrintWriter out = null;
        BufferedReader in = null;
        String result = "";
        try {
            URL realUrl = new URL(url);
            // 打开和URL之间的连接
            URLConnection conn = realUrl.openConnection();
            // 设置通用的请求属性
            conn.setRequestProperty("accept", "*/*");
            conn.setRequestProperty("connection", "Keep-Alive");
            conn.setRequestProperty("user-agent",
                    "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Content-Type",
                    "application/json;charset=UTF-8");
            // 发送POST请求必须设置如下两行
            conn.setDoOutput(true);
            conn.setDoInput(true);
            // 获取URLConnection对象对应的输出流
            out = new PrintWriter(conn.getOutputStream());
            // 发送请求参数
            out.print(param);
            // flush输出流的缓冲
            out.flush();
            // 定义BufferedReader输入流来读取URL的响应
            in = new BufferedReader(new InputStreamReader(
                    conn.getInputStream(), "UTF-8"));
            String line;
            while ((line = in.readLine()) != null) {
                result += line;
            }

        } catch (Exception e) {
            System.out.println("发送 POST 请求出现异常！" + e);
            e.printStackTrace();
        }
        // 使用finally块来关闭输出流、输入流
        finally {
            try {
                if (out != null) {
                    out.close();
                }
                if (in != null) {
                    in.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return result;
    }

    public static void main(String[] args) {

        try {

            String sr;
            Gson obj = new Gson();
            Map<String, List> map;
            LinkedHashMap<String, String> manager = new LinkedHashMap<String, String>();
            String managerName;
            String url;
            for (int page = 0; page < 236; page++) {
                Thread.sleep(100);
                sr = sendPost(
                        "http://gs.amac.org.cn/amac-infodisc/api/pof/manager?rand=0.9600556030665812&page="
                                + page + "&size=100", "{}");

                map = obj.fromJson(sr, Map.class);
                List li = map.get("content");

                for (int i = 0; i < li.size(); i++) {//
                    LinkedTreeMap m = (LinkedTreeMap) li.get(i);
                    managerName = (String) m.get("managerName");// 私募基金管理人name
                    url = (String) m.get("url"); // 私募基金管理人url
//					System.out.println(managerName + url);
                    manager.put(managerName, url);
                }
            }
//			GetBeans g = new GetBeans();
//			g.getall(manager);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
