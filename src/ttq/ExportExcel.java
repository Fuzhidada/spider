package ttq;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class ExportExcel {
    public boolean exportExcel(List<String> li) {
        HSSFWorkbook workbook = new HSSFWorkbook(); // 创建工作簿对象
        HSSFSheet sheet = workbook.createSheet("test"); // 创建工作表

        HSSFCellStyle style = workbook.createCellStyle();
        style.setBorderRight(HSSFCellStyle.BORDER_THIN);
        sheet.setColumnWidth(5, 256 * 20);
        sheet.setColumnWidth(8, 256 * 100);
        // 产生表格标题行
        HSSFRow rowm = sheet.createRow(5);
        HSSFCell cellTiltle = rowm.createCell(5);
//
        cellTiltle.setCellValue("各种url");

        for (int i = 1; i <= li.size(); i++) {
            HSSFRow rowmi = sheet.createRow(i);
            HSSFCell cellTiltlei = rowmi.createCell(8);
            cellTiltlei.setCellValue(li.get(i - 1));
        }
        /*
         * for(int i=0;i<li.size();i++){ System.out.println(li.get(i)); }
         */

        FileOutputStream out = null;
        File f = new File("D:/works");
        f.mkdir();
        File ff = new File(f, "url.xls");
        try {
            ff.createNewFile();
            out = new FileOutputStream(ff);
            workbook.write(out);
            workbook.close();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }
}
