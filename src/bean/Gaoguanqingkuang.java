package bean;

import java.io.Serializable;

/**
 * Model class of gaoguanqingkuang.
 *
 * @author generated by ERMaster
 * @version $Id$
 */
public class Gaoguanqingkuang implements Serializable {

    /**
     * serialVersionUID.
     */
    private static final long serialVersionUID = 1L;

    /**
     * name.
     */
    private String name;

    /**
     * zhiwu.
     */
    private String zhiwu;

    /**
     * shifouyoucongyezige.
     */
    private String shifouyoucongyezige;

    /**
     * 私募基金管理人基本信息.
     */
    private String baseMessage;

    /**
     * Constructor.
     */
    public Gaoguanqingkuang() {
    }

    /**
     * Get the name.
     *
     * @return name
     */
    public String getName() {
        return this.name;
    }

    /**
     * Set the name.
     *
     * @param name name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get the zhiwu.
     *
     * @return zhiwu
     */
    public String getZhiwu() {
        return this.zhiwu;
    }

    /**
     * Set the zhiwu.
     *
     * @param zhiwu zhiwu
     */
    public void setZhiwu(String zhiwu) {
        this.zhiwu = zhiwu;
    }

    /**
     * Get the shifouyoucongyezige.
     *
     * @return shifouyoucongyezige
     */
    public String getShifouyoucongyezige() {
        return this.shifouyoucongyezige;
    }

    /**
     * Set the shifouyoucongyezige.
     *
     * @param shifouyoucongyezige shifouyoucongyezige
     */
    public void setShifouyoucongyezige(String shifouyoucongyezige) {
        this.shifouyoucongyezige = shifouyoucongyezige;
    }

    /**
     * Get the 私募基金管理人基本信息.
     *
     * @return 私募基金管理人基本信息
     */
    public String getBaseMessage() {
        return this.baseMessage;
    }

    /**
     * Set the 私募基金管理人基本信息.
     *
     * @param baseMessage 私募基金管理人基本信息
     */
    public void setBaseMessage(String baseMessage) {
        this.baseMessage = baseMessage;
    }


}
